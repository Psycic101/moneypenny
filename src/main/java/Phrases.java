import java.util.*;

/**
 * Created by Siphamandla on 8/21/2016.
 */
class Phrases {

    private static Map<String, ArrayList<String>> Phrases = new HashMap<>();
    private static ArrayList<String> newList=null;

    static String getPhrase(String listName) {
        Phrases.clear();
        Phrases = new HashMap<>();
        AddLists();

        String phrase = "";

        Iterator entries = Phrases.entrySet().iterator();
        while (entries.hasNext()) {
            Map.Entry thisEntry = (Map.Entry) entries.next();

            Object key = thisEntry.getKey();
            if (key.equals(listName)) {
                Random rn = new Random();
                int minimum = 0;
                ArrayList curList = Phrases.get(key);
                int range = curList.size() - minimum + 1;
                int x = rn.nextInt(range - 1);
                phrase = (String) curList.get(x);
            }
        }

        return phrase;
    }

    private static void AddLists() {
        AddMilestonePhrases();
        AddWelcomeMessages();
        PopulateDecisionNo();
        PopulateDecisionYes();
        PopulateVerifyMilestoneDate();
        PopulateGotMilestoneCost();
        PopulateVerifyingMilestoneDate();
        PopulateNewGoalAddedPhrases();
        DoesUserKnowDate();

    }

    /*
    Use AddMileStone to randomise these phrases
     */
    private static void AddMilestonePhrases() {
        newList = new ArrayList<>();
        newList.add("and what would you like to call it");
        newList.add("and what are you calling it");
        newList.add("what must i save it as");
        newList.add("and what are you going to call this goal");//
        newList.add("what would you like to call the goal");//item->goal
        newList.add("provide me with a title for this goal");//item->goal
        newList.add("what is the name of this goal you'd like to achieve");////////////
        newList.add("and what must i call this goal");//item->goal
        newList.add("what would you like the title of this goal to be");/////
        newList.add("and what would the name be");//

        Phrases.put("AddMileStone", newList);
    }

    /*
    Use Welcome to randomise these phrases
     */
    private static void AddWelcomeMessages() {
        newList = new ArrayList<>();
        newList.add("hi there my name is Penny");///////isn't the name Penny
        newList.add("welcome to Money Penny");///// same thing
        newList.add("what can i help you with");
        newList.add("what's up? ");
        newList.add("hey there");///
        newList.add("hey what's going on");///???
        newList.add("hey there what's up");///

        Phrases.put("Welcome", newList);
    }

    /*
    Use DecisionNo
     */
    private static void PopulateDecisionNo() {
        newList = new ArrayList<>();
        newList.add("schweet, do you have an idea of how much you want to pay towards it, each month");
        newList.add("and how much exactly do you want to save on a monthly basis");
        newList.add("how much did you have in mind for this?");
        newList.add("how much monthly payment would like to provide for this goal?");
        newList.add("how much money are you willing to part with per month for this goal");
        newList.add("how much money are you willing to part with monthly to achieve this goal");//
        newList.add("how much money are you willing to part with on a monthly basis for your goal");//

        Phrases.put("DecisionNo", newList);
    }

    /*
    Use DecisionYes
     */
    private static void PopulateDecisionYes() {
        newList = new ArrayList<>();
        newList.add("please, oh master, tell me when");
        newList.add("oh please tell me when");
        newList.add("and when will this happen?");///////
        newList.add("and when do you want it");//
        newList.add("and when are you looking to achieve this");//
        newList.add("and when do you expect to get this");//
        newList.add("when would you like to achieve this by");//

        Phrases.put("DecisionYes", newList);
    }

    /*
    Use PopulateVerifyMilestoneDate
     */
    private static void PopulateVerifyMilestoneDate() {

        newList = new ArrayList<>();
        newList.add("awesome, i think i'll remember that one");
        newList.add("i will definitely remember that");
        newList.add("rodger that");
        newList.add("message received");
        newList.add("your goal has been noted");//message->goal
        newList.add("fantastic, i'm looking forward to helping you achieve this goal");///////////
        newList.add("great, I can help you with that goal");//??? can <-> will
        newList.add("I will definitely help you achieve that");///that <-> this <-> your goal

        Phrases.put("PopulateVerifyMilestoneDate", newList);
    }

    /*
    GotMilestoneCost
     */
    private static void PopulateGotMilestoneCost() {

        newList = new ArrayList<>();
        newList.add("schweet, do you have an idea of how much you want to pay towards it, each month");
        newList.add("cool, do you have an idea of how much you want to pay towards it, each month");
        newList.add("i hear you, do you have an idea of how much you want to pay towards it, each month");
        newList.add("okay, do you have a monthly payment in mind for this goal?");
        newList.add("so do you know the monthly payment you would like to provide");


        Phrases.put("GotMilestoneCost", newList);
    }
    /*
    VerifyingMilestoneDate
     */
    private static void PopulateVerifyingMilestoneDate() {

        newList = new ArrayList<>();
        newList.add("when would you like it");
        newList.add("on which date would you like to purchase the item");
        newList.add("please tell me when you would like to purchase the item");

        Phrases.put("VerifyingMilestoneDate", newList);
    }

    /*
    Use NewGoalAddedPhrases
     */
    private static void PopulateNewGoalAddedPhrases()
    {
        newList = new ArrayList<>();
        newList.add("ka ching, one more goal in the bag");
        newList.add("your goal has been saved");
        newList.add("yay, another goal to be achieved");

        Phrases.put("NewGoalAddedPhrases", newList);
    }

    private static void DoesUserKnowDate()
    {
        newList = new ArrayList<>();
        newList.add("Do you know the date");
        newList.add("Do you have a date in mind");

        Phrases.put("DoesUserKnowDate", newList);
    }

    private static ArrayList<String>AboutPenny() {
        ArrayList<String> list = new ArrayList<>();
        list.add(" my future is pretty bright if you ask me. ");
        list.add(" after some training i will able to help people recover from debt. ");
        list.add(" i'm really interested in helping people set up and manage a budget plan. ");
        list.add(" if you let me into your home, i guarantee that your families finances will be well managed. ");
        list.add(" If you like i can make friends with your bank account, in order to manage your finances better. ");
        return list;
    }

}
