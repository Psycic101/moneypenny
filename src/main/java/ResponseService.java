import org.json.JSONObject;

import java.security.cert.PKIXRevocationChecker;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

public class ResponseService {
    private static String loggedName = "Unknown";
    private static PennyStates currentState = PennyStates.Main;
    private static boolean endSession = false;
    private static String outputSpeechText = "Sorry, I am still a work in progress. Please try again later.";
    private static String outputSpeechType = "PlainText";
    private static String id = "9206150215089";

    private static String curMilestoneName = "";
    private static int curMilestoneCost = -1;
    private static int curMilestoneMonthlyPayment = -1;
    private static Date curMilestoneTargetDate = null;

    // Get request type from JSON request
    public static String getRequestType(JSONObject obj) {

        JSONObject requestTypeObject = obj.getJSONObject("request");

        return requestTypeObject.getString("type");
    }

    // Get slot value for the IntentRequest
    public static String getSlotValue(JSONObject obj, String intentName) {

        JSONObject requestTypeObject = obj.getJSONObject("request");
        JSONObject intentObject = requestTypeObject.getJSONObject("intent");
        JSONObject slotsObject = intentObject.getJSONObject("slots");

        return slotsObject.getJSONObject(intentName).getString("value");
    }

    public static String getIntentNameValue(JSONObject obj) {
        JSONObject requestTypeObject = obj.getJSONObject("request");
        JSONObject intentObject = requestTypeObject.getJSONObject("intent");
        return intentObject.getString("name");
    }

    // Construct the JSON response sent back to the skill service
    public static JSONObject constructResponseObject(JSONObject obj, String requestType) {

        outputSpeechText = "";
        outputSpeechType = "PlainText";
        if (requestType.equals("LaunchRequest")) {
            launchIntent();
        } else if (requestType.equals("IntentRequest")) {
            // Case through each "IntentID" (intent >> name) to determine what code to do.
            String intentName = getIntentNameValue(obj);
            System.out.println("Received intent kind: " + intentName);
            // switch on State enum to allow only valid intents per current state.
            if (Objects.equals(intentName, "DecisionBack"))
                decisionBack();
            else
                switch (currentState) {
                    case Main:
                        switch (intentName) {
                            case "AddMilestone":
                                addMilestoneIntent();
                                break;
                            case "LogInAs":
                                logInAsIntent(obj);
                                break;
                            case "GetCurrentState":
                                getCurrentState();
                                break;
                            case "ViewMilestones":
                                viewMilestones();
                                break;
                            case "DeleteMilestone":
                                deleteMilestoneIntent(obj);
                        }
                        break;
                    case AddedMilestone:
                        switch (intentName) {
                            case "GetName":
                                getName(obj);
                                break;
                        }
                        break;
                    case GotMilestoneName:
                        switch (intentName) {
                            case "GetAmount":
                                getCost(obj);
                                break;
                        }
                        break;
                    case GotMilestoneCost:
                        switch (intentName) {
                            case "GetDate":
                                getDate(obj);
                                break;
                            case "DecisionYes":
                                decisionYes();
                                break;
                            case "DecisionNo":
                                decisionNo();
                                break;
                        }
                        break;
                    case GotMilestoneDate:
                        switch (intentName) {
                            case "GetDate":
                                getDate(obj);
                                break;
                        }
                        break;
                    case VerifyingMilestoneDate:
                        switch (intentName) {
                            case "DecisionYes":
                                decisionYes();
                                break;
                            case "DecisionNo":
                                decisionNo();
                                break;
                        }
                        break;
                    case GotMilestoneAmount:
                        switch (intentName) {
                            case "GetAmount":
                                getAmount(obj);
                                break;
                        }
                        break;
                    case ShowPlans:
                        //3 choices
                        break;
                    case PickedPlan:
                        switch (intentName) {
                            case "DecisionYes":
                                decisionYes();
                                break;
                            case "DecisionNo":
                                decisionNo();
                                break;
                        }
                        break;
                    case DeleteMilestone:
                        switch (intentName) {
                            case "DecisionYes":
                                decisionYes();
                                break;
                            case "DecisionNo":
                                decisionNo();
                                break;
                        }
                        break;
                    case VerifyingMonthlyMilestoneAmount: {
                        switch (intentName) {
                            case "DecisionYes":
                                decisionYes();
                                break;
                            case "DecisionNo":
                                decisionNo();
                                break;
                        }
                        break;
                    }

                    case DoesntWantToSpecifyDate: {
                        switch (intentName) {
                            case "DecisionYes":
                                decisionYes();
                                break;
                            case "DecisionNo":
                                decisionNo();
                                break;
                        }
                        break;
                    }
                    case Register:
                        break;
                }
        } else {

            // Blank response for SessionEndRequest
            outputSpeechText = "<speak>Goodbye, " + loggedName + "</speak>";
            outputSpeechType = "SSML";
            System.out.println("Blank response reached");
            endSession = true;
        }

        // Construct JSON response package
        JSONObject outputSpeechElement = new JSONObject();
        outputSpeechElement.put("type", outputSpeechType);

        if (outputSpeechType.equals("SSML")) {
            outputSpeechElement.put("ssml", outputSpeechText);
        } else {
            outputSpeechElement.put("text", outputSpeechText);
        }

        JSONObject outputSpeech = new JSONObject();
        outputSpeech.put("outputSpeech", outputSpeechElement);

        outputSpeech.put("shouldEndSession", endSession);


        JSONObject responseObject = new JSONObject();
        responseObject.put("response", outputSpeech);

        return responseObject;
    }

    private static void writeMilestoneToDB(String name, int cost, int monthlyPayment, java.sql.Date currentDate, java.sql.Date targetDate) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("Where is your MySQL JDBC Driver?");
            e.printStackTrace();
            return;
        }
        System.out.println("MySQL JDBC Driver Registered!");
        Connection connection = null;
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mpdb", "penny", "penny");

        } catch (SQLException e) {
            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
            return;
        }

        if (connection != null) {
            System.out.println("You made it, take control of your database now!");
            PreparedStatement preparedStatement = null;
            try {
                preparedStatement = connection.prepareStatement("INSERT INTO mpdb.milestone VALUES (?, ?, ?, ?, ?, ?)");
                preparedStatement.setString(1, id);
                preparedStatement.setString(2, name);
                preparedStatement.setInt(3, cost);
                preparedStatement.setInt(4, monthlyPayment);
                preparedStatement.setDate(5, currentDate);
                preparedStatement.setDate(6, targetDate);
                preparedStatement.executeUpdate();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Failed to make connection!");
        }
    }

    //////////////////////
    /// Intent Methods ///
    //////////////////////

    private static void addMilestoneIntent() {
        outputSpeechText = Phrases.getPhrase("AddMileStone");
        outputSpeechType = "PlainText";
        currentState = PennyStates.AddedMilestone;
    }

    private static void deleteMilestoneIntent(JSONObject obj) {
        String name = getSlotValue(obj, "MilestoneDeleteName");
        System.out.println(name);

        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("Where is your MySQL JDBC Driver?");
            e.printStackTrace();
            return;
        }
        System.out.println("MySQL JDBC Driver Registered!");
        Connection connection = null;
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mpdb", "penny", "penny");

        } catch (SQLException e) {
            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
            return;
        }
        if (connection != null) {
            System.out.println("You made it, take control of your database now!");
            PreparedStatement preparedStatement = null;
            try {
                preparedStatement = connection.prepareStatement("DELETE FROM mpdb.milestone WHERE name = ?;");
                preparedStatement.setString(1, name);
                preparedStatement.executeUpdate();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Failed to make connection!");
        }

        outputSpeechText = "<speak>you have removed " + name + " from your goals</speak>";
        outputSpeechType = "SSML";
    }

    private static void logInAsIntent(JSONObject obj) {
        String name = getSlotValue(obj, "UserName");
        loggedName = name;

        outputSpeechText = "<speak>you have logged in as " + name + "</speak>";
        outputSpeechType = "SSML";
    }

    private static void getCurrentState() {

        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("Where is your MySQL JDBC Driver?");
            e.printStackTrace();
            return;
        }
        System.out.println("MySQL JDBC Driver Registered!");
        Connection connection = null;

        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mpdb", "penny", "penny");
        } catch (SQLException e) {
            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
            return;
        }

        if (connection != null) {

            try {

                PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM mpdb.milestone");
                ResultSet resultSet = preparedStatement.executeQuery();
                ArrayList<String> milestone = new ArrayList<>();

                while (resultSet.next()) {

                    String name = resultSet.getString("name");//milestone title
                    String monthlypayment = resultSet.getString("monthpay");
                    String initialdate = resultSet.getString("initialdate");
                    String goal = name + "," + monthlypayment + "," + initialdate;
                    milestone.add(goal);
                    //milestones added
                }
                outputSpeechText = "<speak>According to my diary, your progress should be as follows. ";
                outputSpeechType = "SSML";

                int totalSavings = 0;
                for (int x = 0; x <= milestone.size() - 1; x++) {
                    String milestoneEntry = milestone.get(x);
                    String[] itemsplit = milestoneEntry.split(",");
                    String name = itemsplit[0];
                    Integer progressAmount = Integer.parseInt(itemsplit[1]);
                    int months = 0;
//                    java.util.Date initialDate = resultSet.getDate("initialdate");
                    System.out.println(itemsplit[2]);
                    String sDate = itemsplit[2];
                    int iYear = Integer.parseInt(sDate.substring(0, 3));
                    int iMonth = Integer.parseInt(sDate.substring(5, 6)) - 1;
                    int iDay = Integer.parseInt(sDate.substring(8, 9));
                    Date initialDate = new Date(iYear, iMonth, iDay);
                //    java.util.Date todaysDate = new java.util.Date();
               //     months = Math.abs(initialDate.getMonth() - todaysDate.getMonth());

                    Date dateDiff = new Date(initialDate.getTime() - (new Date()).getTime());
                    months = dateDiff.getMonth();

                    int SavedAmountForItem = progressAmount * months;

                    outputSpeechText += "For the " + name + " goal and a monthly payment of " + progressAmount + " , you should have saved " + SavedAmountForItem + ". ";
                    totalSavings += SavedAmountForItem;
                }

                //now have our estimated total
                PreparedStatement preparedStatement1 = connection.prepareStatement("SELECT * FROM mpdb.stduser");
                ResultSet resultSet1 = preparedStatement1.executeQuery();

                while (resultSet1.next()) {
                    Integer balance = Integer.parseInt(resultSet1.getString("Savings"));
                    if (balance > totalSavings) {
                        outputSpeechText += "your savings balance off " + balance + " is greater than my estimated amount off " + balance + ". I'm impressed. ";
                    } else {
                        outputSpeechText += "your savings balance off " + balance + " is less than my estimated amount off " + balance + ". Something has to change my friend. ";
                    }
                }

                outputSpeechText += "Would you like me to email this overview to your personal email address. </speak>";
                System.out.println(outputSpeechText);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private static void getName(JSONObject obj) {
        curMilestoneName = getSlotValue(obj, "MilestoneName");

        outputSpeechText = "how much will it cost";
        outputSpeechType = "PlainText";
        currentState = PennyStates.GotMilestoneName;
    }

    private static void decisionBack() {
        outputSpeechText = "Okay";
        outputSpeechType = "PlainText";

        switch (currentState) {
            case AddedMilestone:
                currentState = PennyStates.Main;
                break;
            case GotMilestoneName:
                addMilestoneIntent();
                break;
            case GotMilestoneCost:
                outputSpeechText = "okay, so what does it cost";
                outputSpeechType = "PlainText";
                currentState = PennyStates.GotMilestoneName;
                break;
            case GotMilestoneDate:
                outputSpeechText = "do you know when you'd like it";
                outputSpeechType = "PlainText";
                currentState = PennyStates.GotMilestoneCost;
                break;
            case VerifyingMilestoneDate:
                outputSpeechText = "so then, when would you like it";
                outputSpeechType = "PlainText";
                currentState = PennyStates.GotMilestoneDate;
                break;
            case GotMilestoneAmount:
                outputSpeechText = "well, do you have an idea of how much you'd like to pay per month for it";
                outputSpeechType = "PlainText";
                currentState = PennyStates.DoesntWantToSpecifyDate;
                break;
            case ShowPlans: //exact same as above since they come from same root
                outputSpeechText = "do you have an idea of how much you'd like to pay per month for it";
                outputSpeechType = "PlainText";
                currentState = PennyStates.GotMilestoneCost;
                break;
            case PickedPlan:
                ExploreOptions();
                outputSpeechType = "PlainText";
                currentState = PennyStates.ShowPlans;
                break;
            case VerifyingMonthlyMilestoneAmount:
                outputSpeechText = "how much would you like to save per month";
                outputSpeechType = "PlainText";
                currentState = PennyStates.GotMilestoneAmount;
                break;
        }
    }

    private static void getCost(JSONObject obj) {
        curMilestoneCost = Integer.parseInt(getSlotValue(obj, "Amount"));

        outputSpeechText = Phrases.getPhrase("DoesUserKnowDate");
        outputSpeechType = "PlainText";
        currentState = PennyStates.GotMilestoneCost;
    }

    private static void getDate(JSONObject obj) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        try {
            curMilestoneTargetDate = formatter.parse(getSlotValue(obj, "MilestoneDate"));
            System.out.println(curMilestoneTargetDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date dateDiff = new Date(curMilestoneTargetDate.getTime() - (new Date()).getTime());
        int monthsFromNow = dateDiff.getMonth();

        outputSpeechText = "<speak>sweet. my internal calculator tells me that it would cost you " + curMilestoneCost / monthsFromNow + " dollars per month. is this okay</speak>";
        outputSpeechType = "SSML";
        currentState = PennyStates.VerifyingMilestoneDate;

        curMilestoneMonthlyPayment = curMilestoneCost / monthsFromNow;
    }

    private static void decisionYes() {
        switch (currentState) {
            case GotMilestoneCost:
                //outputSpeechText = Phrases.getPhrase("DecisionYes");
                outputSpeechText = "Cool, when would you like it";
                outputSpeechType = "PlainText";
                currentState = PennyStates.GotMilestoneDate;
                break;
            case VerifyingMilestoneDate:
                System.out.println("VerifyingMilestoneDate");
                writeMilestoneToDB(curMilestoneName, curMilestoneCost, curMilestoneMonthlyPayment, new java.sql.Date(new java.util.Date().getTime()), new java.sql.Date(curMilestoneTargetDate.getTime()));
                outputSpeechText = Phrases.getPhrase("PopulateVerifyMilestoneDate");
                outputSpeechType = "PlainText";
                currentState = PennyStates.Main;
                break;
            case VerifyingMonthlyMilestoneAmount:
                writeMilestoneToDB(curMilestoneName, curMilestoneCost, curMilestoneMonthlyPayment, new java.sql.Date(new java.util.Date().getTime()), new java.sql.Date(curMilestoneTargetDate.getTime()));
                outputSpeechText = Phrases.getPhrase("NewGoalAddedPhrases");
                outputSpeechType = "PlainText";
                currentState = PennyStates.Main;
                break;
            case DoesntWantToSpecifyDate:
                outputSpeechText = "How much do you think it will cost you per month";
                outputSpeechType = "PlainText";
                currentState = PennyStates.GotMilestoneAmount;
                break;

        }

    }

    private static void decisionNo() {

        switch (currentState) {
            case GotMilestoneAmount:
                ExploreOptions();
                outputSpeechType = "PlainText";
                currentState = PennyStates.ShowPlans;
                break;
            case GotMilestoneCost:
                outputSpeechText = Phrases.getPhrase("GotMilestoneCost");
                outputSpeechType = "PlainText";
                currentState = PennyStates.DoesntWantToSpecifyDate;
                break;
            case VerifyingMilestoneDate:
                outputSpeechText = Phrases.getPhrase("VerifyingMilestoneDate");
                outputSpeechType = "PlainText";
                currentState = PennyStates.GotMilestoneDate;
                break;
            case VerifyingMonthlyMilestoneAmount:
                outputSpeechText = "how much would you like to save per month";
                outputSpeechType = "PlainText";
                currentState = PennyStates.GotMilestoneAmount;
                break;
            case PickedPlan:
                ExploreOptions();
                outputSpeechType = "PlainText";
                currentState = PennyStates.ShowPlans;
                break;
        }
    }

    private static void getAmount(JSONObject obj) {
        try {
            curMilestoneMonthlyPayment = Integer.parseInt(getSlotValue(obj, "Amount"));
            System.out.println(curMilestoneMonthlyPayment);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Date newDate = null;

        int months = 0;
        if (curMilestoneMonthlyPayment > 0)
            months = curMilestoneCost / curMilestoneMonthlyPayment;
        else
            months = 1;

        Calendar cal = Calendar.getInstance();
        newDate = new Date();
        cal.setTime(newDate);
        cal.add(Calendar.MONTH, months - 1); //m
        newDate = cal.getTime();


        curMilestoneTargetDate = newDate;

        if (newDate != null) {
            //outputSpeechText = "<speak>sweet sugars. then you should have it on " + newDate.getDay() + " " + getProperMonthName(newDate.getMonth()) + " " + newDate.getYear() + ", are you cool with this?</speak>";
            StringBuilder sb = new StringBuilder();
            sb.append("<speak>sweet sugars. then you should have it on ");
            sb.append("<say-as interpret-as=\"date\">");
            sb.append(getDateString(newDate));
            sb.append("</say-as>");
            sb.append(", are you cool with this?</speak>");
//            outputSpeechText = "<speak>sweet sugars. then you should have it on <say-as interpret-as=\"date\">" + getDateString(newDate) + "</say-as>, are you cool with this?</speak>";
            outputSpeechText = sb.toString();
            System.out.println(outputSpeechText);
            outputSpeechType = "SSML";
            currentState = PennyStates.VerifyingMonthlyMilestoneAmount;
        } else {
            outputSpeechText = "Something went wrong ";
            outputSpeechType = "PlainText";
        }
    }

    private static String getDateString(Date newDate) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(newDate);
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH) + 1;
        String sMonth = "" + month;
        if (month < 10) {
            sMonth = "0" + month;
        }
        return String.valueOf(year) + sMonth + "??";
    }


    public static void ExploreOptions() {

        try {
            System.out.println(curMilestoneMonthlyPayment);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ArrayList<Integer> months = new ArrayList<Integer>();
        ArrayList<String> forecasts = new ArrayList<String>();
        AddIntervals(months);

        boolean limit = false;
        while (limit == false) {
            int counter = 0;
            for (int y = 0; y <= months.size() - 1; y++) {
                int month = months.get(y);
                int answer = curMilestoneCost / month;
                if ((answer > 0) && (counter != months.size())) {
                    //IE you need more than one month
                    String cur = +answer + "," + month;
                    forecasts.add(cur);
                    counter += 1;
                    if (counter == months.size()) {
                        limit = true;
                    }
                } else {
                    //Stop as soon as answer is less than 0
                    return;
                }
            }
        }

        if (forecasts.size() > 0) {
            outputSpeechText = "<speak>";
            for (int y = 0; y <= forecasts.size() - 1; y++) {
                String cur = forecasts.get(y);
                String[] splitItems = cur.split(",");
                int answer = Integer.parseInt(splitItems[0]);
                int Months = Integer.parseInt(splitItems[1]);

                System.out.println("");

                if ((y + 1) == 3)
                    outputSpeechText += "and ";
                outputSpeechText += "Option " + (y + 1) + " : You can save " + answer + " dollars over " + months + " months. ";
                outputSpeechType = "SSML";
                currentState = PennyStates.PickedPlan;
            }
            outputSpeechText += "</speak>";
        }
    }

    private static void viewMilestones() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("Where is your MySQL JDBC Driver?");
            e.printStackTrace();
            return;
        }
        System.out.println("MySQL JDBC Driver Registered!");
        Connection connection = null;
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mpdb", "penny", "penny");

        } catch (SQLException e) {
            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
            return;
        }
        if (connection != null) {
            outputSpeechText = "Sorry, you currently have no goals";
            outputSpeechType = "PlainText";

            try {
                PreparedStatement preparedStatement = connection.prepareStatement("SELECT name FROM mpdb.milestone");
                ResultSet resultSet = preparedStatement.executeQuery();
                StringBuilder sb = new StringBuilder();
                sb.append("You currently have the following goals. ");
                boolean added = false;
                ArrayList<String> names = new ArrayList<>();
                while (resultSet.next()) {
                    names.add(resultSet.getString("name"));
                    added = true;
                }

                if (added) {
                    if (names.size() == 1) {
                        sb.append(names.get(0));
                    } else if (names.size() == 2) {
                        sb.append(names.get(0));
                        sb.append(" and ");
                        sb.append(names.get(1));
                    } else {
                        for (int i = 0; i < names.size(); i++) {
                            sb.append(names.get(i));
                            if (i != names.size() - 2) {
                                sb.append(", ");
                            } else {
                                sb.append(" and ");
                            }
                        }
                    }
                    outputSpeechText = sb.toString();
                    outputSpeechType = "PlainText";
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private static void launchIntent() {
        outputSpeechText = Phrases.getPhrase("Welcome");
        outputSpeechType = "PlainText";
        endSession = false;
    }

    private static void options(int cost) {
        Date todayDate = null;
        int months = 12;

        if (months > 0 && cost > 0) {
            Calendar cal = Calendar.getInstance();
            todayDate = new Date();
            cal.setTime(todayDate);
            cal.add(Calendar.MONTH, months - 1); //m
            todayDate = cal.getTime();

            //First year plan
            int monthlyPaymentAmount = 1111;
        }

    }

    public static String getProperMonthName(int month) {
        String[] monthNames = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
        return monthNames[month];
    }

    private static void AddIntervals(ArrayList lists) {
        lists.add(12);
        lists.add(24);
        lists.add(36);
    }


}
