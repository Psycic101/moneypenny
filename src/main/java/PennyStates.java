/**
 * Created by simon on 2016/08/21.
 */
public enum PennyStates {
    Main,
    AddedMilestone,
    GotMilestoneName,
    GotMilestoneCost,
    GotMilestoneDate,
    VerifyingMilestoneDate,
    VerifyingMonthlyMilestoneAmount,
    GotMilestoneAmount,
    ShowPlans,
    PickedPlan,
    DeleteMilestone,
    Register,
    DoesntWantToSpecifyDate
}
