import org.json.JSONObject;

import static spark.Spark.post;

public class Main {
    public static void main(String[] args) {

        post("/alexa", (request, response) -> {

            // Get request type
            JSONObject requestObject = new JSONObject(request.body());
            String requestType = ResponseService.getRequestType(requestObject);

            // Construct the response
            JSONObject responseJson = ResponseService.constructResponseObject(requestObject, requestType);

            return responseJson;
        });
    }

}
